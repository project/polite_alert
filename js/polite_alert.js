(function ($) {
  Drupal.behaviors.politeAlert = {
    attach: function (context, settings) {
      var _polite = {
        alertBlock: function(){
          // Check for cookie to verify display.
          var alertClosed = $.cookie('polite-alert');
          console.log(alertClosed);
          if(!alertClosed) {
            $('.block-polite-alert').slideDown(600);
            //$('.block-polite-alert button').focus();
          }
          // Close and set cookie when user closes alert.
          $('.block-polite-alert .polite-alert-close').click(function() {
            $.cookie('polite-alert', '1', { path: '/' });
            $('.block-polite-alert').slideUp(600);
          });
        },

        init: function() {
          this.alertBlock();
        }
      }; // end _polite{}

      _polite.init();
    }
  };
}(jQuery));