<?php

namespace Drupal\polite_alert\Plugin\Block;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Polite Alert' Block.
 *
 * @Block(
 *   id = "polite_alert_block",
 *   admin_label = @Translation("Polite Alert block"),
 *   category = @Translation("Polite Alert"),
 * )
 */
class PoliteAlertBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (\Drupal::config('polite_alert.adminsettings')->get('polite_alert_active') === 1) {
      $link = '';
      $allowed_tags = array('b', 'em', 'strong', 'span');
      $alert_label = Xss::filter(\Drupal::config('polite_alert.adminsettings')->get('polite_alert_title'), $allowed_tags);
      $alert_msg = Xss::filter(\Drupal::config('polite_alert.adminsettings')->get('polite_alert_msg'), $allowed_tags);
      $url = \Drupal::config('polite_alert.adminsettings')->get('polite_alert_link_url');
      $link_text = \Drupal::config('polite_alert.adminsettings')->get('polite_alert_link_url');
      $build = [];
      $build['#theme'] = 'polite_alert_content';
      $build['#attached']['library'][] = 'polite_alert/alert-block';
      $build['#alert_msg'] = $alert_msg;
      $build['#alert_label'] = $alert_label;

      return $build;
    }

  }

}