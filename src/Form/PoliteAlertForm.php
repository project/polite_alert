<?php
/**
 * @file
 * Contains Drupal\polite_alert\Form\PoliteAlertForm.
 */
namespace Drupal\polite_alert\Form;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class PoliteAlertForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'polite_alert.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'polite_alert_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('polite_alert.adminsettings');

    $form['polite_alert_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alert Title'),
      '#description' => $this->t('The label/prefix for the alert.'),
      '#default_value' => $config->get('polite_alert_title'),
      '#maxlength' => 128,
      '#size' => 25,
      '#weight' => '0',
    ];
    $form['polite_alert_msg'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alert Message'),
      '#description' => $this->t('The main content for the alert. Recommend maximum length 100 characters.'),
      '#default_value' => $config->get('polite_alert_msg'),
      '#maxlength' => 255,
      '#size' => 100,
      '#weight' => '1',
    ];
    $form['polite_alert_link_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link Title'),
      '#description' => $this->t('The text for the link. Ex: "Learn more"'),
      '#default_value' => $config->get('polite_alert_link_title'),
      '#maxlength' => 128,
      '#size' => 25,
      '#weight' => '2',
    ];
    $form['polite_alert_link_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link URL'),
      '#description' => $this->t('Enter either an internal link starting with &quot;/&quot; or an external link starting with http, https, ftp, or feed.'),
      '#default_value' => $config->get('polite_alert_link_url'),
      '#weight' => '3',
    ];
    $form['polite_alert_active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display the alert.'),
      '#description' => $this->t('Check the box to display alert. Uncheck to hide alert.'),
      '#default_value' => $config->get('polite_alert_active'),
      '#weight' => '4',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('polite_alert.adminsettings')
         ->set('polite_alert_title', $form_state->getValue('polite_alert_title'))
         ->set('polite_alert_msg', $form_state->getValue('polite_alert_msg'))
         ->set('polite_alert_link_title', $form_state->getValue('polite_alert_link_title'))
         ->set('polite_alert_link_url', $form_state->getValue('polite_alert_link_url'))
         ->set('polite_alert_active', $form_state->getValue('polite_alert_active'))
         ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate the URL if it has content.
    if (trim(strlen($form_state->getValue('polite_alert_link_url'))) > 0) {
      $value = trim($form_state->getValue('polite_alert_link_url'));
      $msg = t("The URL %url is not valid. URLs must begin with 'http', 'https', 'ftp', 'feed', '/', '?', or '#'.", array('%url' => $value));
      // External
      if ((strpos($value, 'ftp') === 0) || (strpos($value, 'http') === 0) || (strpos($value, 'https') === 0) || (strpos($value, 'feed') === 0)) {
        if (!UrlHelper::isValid($form_state->getValue('polite_alert_link_url'), $absolute = TRUE)) {
          $form_state->setErrorByName('polite_alert_link_url', $msg);
        }
      }
      // Internal
      else {
        if ((strpos($value, '/') !== 0) && (strpos($value, '#') !== 0) && (strpos($value, '?') !== 0)) {
          $form_state->setErrorByName('polite_alert_link_url', $msg);
        }
      }
    }
  }

}